<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/get/all','Controller@getAll');

$router->get('/get/all/withtrash','Controller@getAllWithTrash');

$router->get('/get/all/trash','Controller@getOnlyTrash');

$router->get('/get/trash/byid/{id}','Controller@getOnlyTrashById');

$router->get('/get/byid/{id}','Controller@getById');

$router->post('/store','Controller@store');

$router->put('/update','Controller@update');

$router->delete('/delete/{id}','Controller@delete');

$router->post('/restore/{id}','Controller@restoreTrash');
