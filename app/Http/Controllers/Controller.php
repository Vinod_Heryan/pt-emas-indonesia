<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    public function getAll(){
        
        $data = Mahasiswa::all();

        $response =
            [
                'message' => 'Get Data Berhasil',
                'data' => $data
            ];
        
            return response()->json($response, 200);

    }

    public function getAllWithTrash(){
        
        $data = Mahasiswa::withTrashed()->get();

        $response =
            [
                'message' => 'Get Data Berhasil',
                'data' => $data
            ];
        
            return response()->json($response, 200);

    }

    public function getOnlyTrash(){
        
        $data = Mahasiswa::onlyTrashed()->get();

        $response =
            [
                'message' => 'Get Data Berhasil',
                'data' => $data
            ];
        
            return response()->json($response, 200);

    }

    public function getOnlyTrashById($id){
        
        $data = Mahasiswa::onlyTrashed()->where('id', $id)->first();

        if(!$data){

            $response =
            [
                'message' => 'Data Tidak Ditemukan',
            ];
        
            return response()->json($response, 404);
        }

        $response =
            [
                'message' => 'Get Data Berhasil',
                'data' => $data
            ];
        
            return response()->json($response, 200);

    }


    public function getById($id){
        
        $data = Mahasiswa::find($id);

        if($data){

            $response =
            [
                'message' => 'Get Data Berhasil',
                'data' => $data
            ];
        
            return response()->json($response, 200);

        }

        $response =
        [
            'message' => 'Data Tidak Ditemukan',
        ];
    
        return response()->json($response, 404);

    }

    public function store(Request $request){
        
        $validator = Validator::make($request->all(),[
            'nim' => 'required|unique:mahasiswa',
            'name' => 'required',
            'jk' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }
            
        try{

            $data = Mahasiswa::create([
                'nim' => $request->nim,
                'name' => $request->name,
                'jk' => $request->jk,
            ]);
        
            $response =
            [
                'message' => 'Tambah Data Berhasil',
                'data' => $data
            ];
        
            return response()->json($response, 201);
            

        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }

    }

    public function update(Request $request){
        
        $validator = Validator::make($request->all(),[
            'id' => 'required',
            'nim' => 'required',
            'name' => 'required',
            'jk' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }

        $find = Mahasiswa::find($request->id);

        if(!$find){

            $response =
            [
                'message' => 'Data Tidak Ditemukan',
            ];
        
            return response()->json($response, 404);

        }
            
        try{

            $data = Mahasiswa::where('id',$request->id)->update([
                'nim' => $request->nim,
                'name' => $request->name,
                'jk' => $request->jk,
            ]);

            $newdata = Mahasiswa::find($request->id);
        
            $response =
            [
                'message' => 'Update Data Berhasil',
                'data' => $newdata
            ];
        
            return response()->json($response, 200);
            

        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }

    }

    public function delete($id){

        try{

            $data = Mahasiswa::find($id);

            if($data){

                Mahasiswa::find($id)->delete();

                $response =
                [
                    'message' => 'Hapus Data Berhasil',
                ];

                return response()->json($response, 200);

            }

            $response =
            [
                'message' => 'Data Tidak Ditemukan',
            ];
        
            return response()->json($response, 404);
            
        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }

    }

    public function restoreTrash($id){

        try{

            $data = Mahasiswa::withTrashed()->where('id', $id);

            if($data){

                Mahasiswa::withTrashed()->where('id', $id)->restore();

                $datarestore = Mahasiswa::find($id);

                $response =
                [
                    'message' => 'Restore Data Berhasil',
                    'data' => $datarestore
                ];

                return response()->json($response, 200);

            }

            $response =
            [
                'message' => 'Data Tidak Ditemukan',
            ];
        
            return response()->json($response, 404);

            
        }catch(QueryException $e){

            return response()->json([
                'massage' => $e->getMessage()
            ],500);

        }

    }
    
}