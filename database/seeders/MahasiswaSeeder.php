<?php

namespace Database\Seeders;

use App\Models\Mahasiswa;
use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nim' => "1234567811",
                'name' => "Vinod",
                'jk' => 'L',
            ],

            [
                'nim' => "1234567822",
                'name' => "Reza",
                'jk' => 'L',
            ],
            [
                'nim' => "1234567833",
                'name' => "Mujur",
                'jk' => 'L',
            ],
            [
                'nim' => "1234567844",
                'name' => "Tina",
                'jk' => 'P',
            ],
        ];
        foreach($data as $key => $value){
            Mahasiswa::create($value);
        }
    }
}
